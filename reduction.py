class Reduction:
    def __init__(self, id, code, qRcode, dateDebut, dateFin, description):
        self.__id = id
        self.__code = code
        self.__qRcode = qRcode
        self.__dateDebut = dateDebut
        self.__dateFin = dateFin
        self.__description = description


    @property
    def id(self):
        return self.__id

    @property
    def code(self):
        return self.__code

    @property
    def qRcode(self):
        return self.__qRcode

    @property
    def dateDebut(self):
        return self.__dateDebut

    @property
    def datefin(self):
        return self.__dateFin

    @property
    def description(self):
        return self.__description

    def to_dict(self):
        return {
            'id': self.id,
            'code': self.code,
            'qRcode': self.qRcode,
            'dateDebut': self.dateDebut,
            'dateFin': self.datefin,
            'description': self.description
        }
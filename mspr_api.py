# coding=utf-8
import sqlite3


class MsprApi:
    def __init__(self, base_name):
        self.conn = sqlite3.connect(base_name)
        self.cursor = self.conn.cursor()

    def drop_database(self, base_name):
        sql_request = f"DROP DATABASE {base_name}"
        self.execute_commande(sql_request)
        self.commit()

    def create_table_reduction(self, table_name):
        sql_request = f"CREATE TABLE IF NOT EXISTS {table_name}(" \
                      f" id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE," \
                      f" qRcode TEXT," \
                      f" code TEXT," \
                      f" dateDebut DATE," \
                      f" dateFin DATE," \
                      f" QRcode TEXT," \
                      f" dateFin DATE," \
                      f" dateDebut DATE," \
                      f" description TEXT)"
        self.execute_commande(sql_request)

    def execute_commande(self, sql_request):
        self.cursor.execute(sql_request)

    def commit(self):
        self.conn.commit()

    def data_to_serialize(self, reduction):
        for row in reduction:
            to_serialize = {
                'id': row[0],
                'qRCode': row[1],
                'code': row[2],
                'dateDebut': row[3],
                'dateFin': row[4],
                'description': row[5],
            }
        return to_serialize

    def data_to_serialize_code(self, reduction):
        for row in reduction:
            to_serialize = {
                'code': row[0]
            }
        return to_serialize

    def insert_in_database(self, reduc):
        self.cursor.execute(
            "INSERT INTO Reduction(code, qRcode, dateDebut, dateFin, description)"
            "VALUES(:code, :qRcode, :dateDebut, :dateFin, :description)", reduc)

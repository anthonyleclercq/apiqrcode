# mspr-api

Utilisation du python:
*   20.000 utilisateurs, python --> robuste
*   Léger et souple
*   API de type REST
*   pas besoin de formation (connaissance de la technique déjà acquis)

<br>

Install:
*  Télécharger l'installeur python 3.7 (lien: https://www.python.org/downloads/release/python-377/)
*  Télécharger un IDE python (pycharm, edupython, ...)

Run:
*  Clone le projet dans un dossier avec soit le lien ssh, soit le lien https (le https est conseillé)
*  Ou simplement télécharger le zip et le dézipper dans un dossier
*  Ouvvrir l'IDE et ouvrir le dossier dézipper précedement 
*  Le projet est prêt à être utilisé

<br>

Ressources base de données:<br>
Les informations concernant le schéma, les scripts et les données de la bdd seront aussi présent à la racine dans un dossier.
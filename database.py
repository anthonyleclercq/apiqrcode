from mspr_api import MsprApi

# Creation de la base, de ces tables et de ces données
bdd = MsprApi("mspr_bdd.db")
bdd.create_table_reduction("Reduction")
bdd.insert_in_database({'qRcode': 'toto20', 'code': '20%', 'dateDebut': '01/01/2020', 'dateFin': '02/02/2020', 'description': '-20% sur toutes la gamme cp'})
bdd.insert_in_database({'qRcode': 'tata40', 'code': '40%', 'dateDebut': '01/07/2020', 'dateFin': '01/08/2020', 'description': '-40% sur toutes la gamme cc'})
bdd.insert_in_database({'qRcode': 'titi60', 'code': '60%', 'dateDebut': '01/08/2020', 'dateFin': '01/10/2020', 'description': '-60% sur toutes la gamme cf'})
bdd.commit()

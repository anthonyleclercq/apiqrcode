from flask_restplus import Resource, Api, fields
from flask import Flask, jsonify, request
from mspr_api import MsprApi
from reduction import Reduction

app = Flask(__name__)
api = Api(app)

chemin_bdd = "D:/antho/Documents/EPSI B3/MSPR/mobile/mspr-api_prod/mspr_bdd.db"

class Reductions(Resource):
    @api.response(200, 'Ok')
    def get(self):
        try:
            list = []
            mspr_api = MsprApi(chemin_bdd)
            mspr_api.execute_commande("SELECT id, qRcode, code, dateDebut, dateFin, description FROM Reduction")
            for row in mspr_api.cursor:
                list.append(Reduction(row[0], row[1], row[2], row[3], row[4], row[5]).to_dict())
        except:
            return "Il n'y a aucune donnée !"
        return jsonify(list)


class ReductionAdd(Resource):
    @api.response(201, 'Ok')
    def post(self):
        code_reduction = request.get_json()
        mspr_api = MsprApi(chemin_bdd)
        mspr_api.insert_in_database(code_reduction)
        mspr_api.commit()
        return 201


class ReductionId(Resource):
    @api.response(200, 'Ok')
    def get(self, id):
        mspr_api = MsprApi(chemin_bdd)
        mspr_api.execute_commande("SELECT id, qRcode, code, dateDebut, dateFin, description from Reduction where id='" + id + "'")
        return jsonify(mspr_api.data_to_serialize(mspr_api.cursor))


class CodeId(Resource):
    @api.response(200, 'Ok')
    def get(self, qrCode):
        mspr_api = MsprApi(chemin_bdd)
        mspr_api.execute_commande("SELECT code from Reduction where QRcode='" + qrCode + "'")
        return jsonify(mspr_api.data_to_serialize_code(mspr_api.cursor))


api.add_resource(Reductions, '/api/reduction/all')
api.add_resource(ReductionAdd, '/api/reduction/postReduction')
api.add_resource(ReductionId, '/api/reduction/<id>')
api.add_resource(CodeId, '/api/reduction/code/<qrCode>')

if __name__ == '__main__':
    app.run(debug=True)
